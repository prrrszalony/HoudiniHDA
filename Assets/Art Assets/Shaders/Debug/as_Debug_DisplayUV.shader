// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Debug/Display UVs"
{
	Properties
	{
		[IntRange]_DisplayUV("Display UV", Range( 0 , 3)) = 0
		[Toggle]_UVGrid("UV Grid", Float) = 1
		_UVgridscale("UV grid scale", Float) = 0
		_Gridstepsintensity("Grid steps intensity", Range( 0 , 1)) = 0.2
		_Gridgradientintensity("Grid gradient intensity", Range( 0 , 1)) = 0.2
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] _texcoord4( "", 2D ) = "white" {}
		[HideInInspector] _texcoord3( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
			float2 uv2_texcoord2;
			float2 uv3_texcoord3;
			float2 uv4_texcoord4;
		};

		uniform float _DisplayUV;
		uniform float _UVgridscale;
		uniform float _Gridstepsintensity;
		uniform float _Gridgradientintensity;
		uniform float _UVGrid;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float temp_output_73_0 = ceil( _DisplayUV );
			float3 appendResult49 = (float3(( temp_output_73_0 + 0 ) , ( temp_output_73_0 + -1 ) , ( temp_output_73_0 + -2 )));
			float3 clampResult74 = clamp( appendResult49 , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float2 uv_TexCoord24 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 uv2_TexCoord27 = i.uv2_texcoord2 * float2( 1,1 ) + float2( 0,0 );
			float2 uv3_TexCoord34 = i.uv3_texcoord3 * float2( 1,1 ) + float2( 0,0 );
			float2 uv4_TexCoord36 = i.uv4_texcoord4 * float2( 1,1 ) + float2( 0,0 );
			float3 layeredBlendVar31 = clampResult74;
			float2 layeredBlend31 = ( lerp( lerp( lerp( uv_TexCoord24 , uv2_TexCoord27 , layeredBlendVar31.x ) , uv3_TexCoord34 , layeredBlendVar31.y ) , uv4_TexCoord36 , layeredBlendVar31.z ) );
			float temp_output_81_0 = ( layeredBlend31.x * _UVgridscale );
			float temp_output_82_0 = ( layeredBlend31.y * _UVgridscale );
			float clampResult124 = clamp( ( ( round( frac( temp_output_81_0 ) ) * 0.75 * _Gridstepsintensity ) + ( ( floor( temp_output_81_0 ) / _UVgridscale ) * 0.75 * _Gridgradientintensity ) + ( round( frac( temp_output_82_0 ) ) * _Gridstepsintensity ) + ( ( floor( temp_output_82_0 ) / _UVgridscale ) * _Gridgradientintensity ) ) , 0 , 1 );
			float3 appendResult123 = (float3(layeredBlend31 , ( clampResult124 * lerp(0,1,_UVGrid) )));
			o.Emission = pow( appendResult123 , 2.2 );
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
1927;29;1906;1004;2292.134;1068.283;2.200818;True;False
Node;AmplifyShaderEditor.RangedFloatNode;32;-1882.264,-175.7286;Float;False;Property;_DisplayUV;Display UV;0;1;[IntRange];Create;True;0;0;0;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.CeilOpNode;73;-1590.336,-171.5736;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-1356.415,-172.9387;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;-1357.727,-84.04196;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-1358.37,-265.1085;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;49;-1137.132,-197.2107;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;24;-1156.389,-49.22922;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-928.9404,80.48337;Float;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;36;-700.9883,348.2272;Float;False;3;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;74;-971.7473,-196.6591;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;-824.1599,214.6365;Float;False;2;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LayeredBlendNode;31;-319.9156,79.605;Float;False;6;0;FLOAT3;0,0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-126.9105,627.1618;Float;False;Property;_UVgridscale;UV grid scale;2;0;Create;True;0;0;12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;84;-86.22635,190.1827;Float;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;355.6511,193.0719;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;347.2958,452.4487;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;106;526.6299,583.1599;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;94;516.9265,195.0262;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;95;516.322,457.0708;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;108;514.7595,292.9893;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;109;664.7149,318.7623;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RoundOpNode;96;672.9265,196.0262;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;113;-110.3847,295.2164;Float;False;Property;_Gridstepsintensity;Grid steps intensity;3;0;Create;True;0;0.2;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;112;-98.78186,390.7895;Float;False;Property;_Gridgradientintensity;Grid gradient intensity;4;0;Create;True;0;0.2;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;107;668.0069,603.4962;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RoundOpNode;99;683.1149,460.3123;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;98;920.2518,463.9034;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;110;913.0957,321.7307;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;913.3023,212.1515;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;934.2329,568.9694;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;104;1273.029,320.2226;Float;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;90;1349.075,467.2087;Float;False;Property;_UVGrid;UV Grid;1;0;Create;True;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;124;1436.129,311.3837;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;125;1625.445,308.9202;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;123;1806.766,97.9416;Float;False;FLOAT3;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;121;1954.937,96.67763;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;2.2;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;78;2187.928,53.23344;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Debug/Display UVs;False;False;False;False;True;True;True;True;True;True;True;True;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;73;0;32;0
WireConnection;60;0;73;0
WireConnection;66;0;73;0
WireConnection;64;0;73;0
WireConnection;49;0;64;0
WireConnection;49;1;60;0
WireConnection;49;2;66;0
WireConnection;74;0;49;0
WireConnection;31;0;74;0
WireConnection;31;1;24;0
WireConnection;31;2;27;0
WireConnection;31;3;34;0
WireConnection;31;4;36;0
WireConnection;84;0;31;0
WireConnection;81;0;84;0
WireConnection;81;1;83;0
WireConnection;82;0;84;1
WireConnection;82;1;83;0
WireConnection;106;0;82;0
WireConnection;94;0;81;0
WireConnection;95;0;82;0
WireConnection;108;0;81;0
WireConnection;109;0;108;0
WireConnection;109;1;83;0
WireConnection;96;0;94;0
WireConnection;107;0;106;0
WireConnection;107;1;83;0
WireConnection;99;0;95;0
WireConnection;98;0;99;0
WireConnection;98;1;113;0
WireConnection;110;0;109;0
WireConnection;110;2;112;0
WireConnection;97;0;96;0
WireConnection;97;2;113;0
WireConnection;111;0;107;0
WireConnection;111;1;112;0
WireConnection;104;0;97;0
WireConnection;104;1;110;0
WireConnection;104;2;98;0
WireConnection;104;3;111;0
WireConnection;124;0;104;0
WireConnection;125;0;124;0
WireConnection;125;1;90;0
WireConnection;123;0;31;0
WireConnection;123;2;125;0
WireConnection;121;0;123;0
WireConnection;78;2;121;0
ASEEND*/
//CHKSM=C2D187D0AF3A3BE896381203C705EF9A011C8CCA