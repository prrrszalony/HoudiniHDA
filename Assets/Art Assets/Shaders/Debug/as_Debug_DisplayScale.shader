// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Debug/Display Scale"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			fixed filler;
		};

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float ifLocalVar110 = 0;
			if( ase_objectScale.x == 1.0 )
				ifLocalVar110 = 1.0;
			else
				ifLocalVar110 = 0.0;
			float ifLocalVar113 = 0;
			if( ase_objectScale.y == 1.0 )
				ifLocalVar113 = 1.0;
			else
				ifLocalVar113 = 0.0;
			float ifLocalVar114 = 0;
			if( ase_objectScale.z == 1.0 )
				ifLocalVar114 = 1.0;
			else
				ifLocalVar114 = 0.0;
			float3 appendResult116 = (float3(ifLocalVar110 , ifLocalVar113 , ifLocalVar114));
			float3 clampResult133 = clamp( appendResult116 , float3( -1,-1,-1 ) , float3( 1,1,1 ) );
			o.Emission = clampResult133;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
1927;29;1906;1004;2154.921;793.1704;1.4091;True;False
Node;AmplifyShaderEditor.RangedFloatNode;131;-1178.416,-66.0749;Float;False;Constant;_Float4;Float 4;1;0;Create;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;132;-1171.37,224.1996;Float;False;Constant;_Float5;Float 5;1;0;Create;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;130;-1186.87,-360.5767;Float;False;Constant;_Float3;Float 3;1;0;Create;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;129;-1172.779,146.6993;Float;False;Constant;_Float2;Float 2;1;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;128;-1181.234,-139.348;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;127;-1188.279,-440.8954;Float;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectScaleNode;81;-1713.274,-249.7951;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ConditionalIfNode;110;-927.1517,-452.6824;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;113;-899.907,-189.1104;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;114;-905.7428,101.3528;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;116;-472.8835,-216.4559;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;133;-293.5006,-218.2577;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;-1,-1,-1;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;80;-51.11332,-266.0922;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Debug/Display Scale;False;False;False;False;True;True;True;True;True;True;True;True;False;False;False;False;False;False;False;Off;0;0;False;0;0;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;110;0;81;1
WireConnection;110;1;130;0
WireConnection;110;2;127;0
WireConnection;110;3;130;0
WireConnection;110;4;127;0
WireConnection;113;0;81;2
WireConnection;113;1;131;0
WireConnection;113;2;128;0
WireConnection;113;3;131;0
WireConnection;113;4;128;0
WireConnection;114;0;81;3
WireConnection;114;1;132;0
WireConnection;114;2;129;0
WireConnection;114;3;132;0
WireConnection;114;4;129;0
WireConnection;116;0;110;0
WireConnection;116;1;113;0
WireConnection;116;2;114;0
WireConnection;133;0;116;0
WireConnection;80;2;133;0
ASEEND*/
//CHKSM=0043019C35E0C53B4351CB00A19860C89225FE62