// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Debug/Display VC"
{
	Properties
	{
		[Toggle]_SingleRGBMode("Single RGB Mode", Float) = 1
		[IntRange]_DisplaySingleRGB("Display Single RGB", Range( 0 , 3)) = 0
		[Toggle]_R("R", Float) = 0
		[Toggle]_G("G", Float) = 0
		[Toggle]_B("B", Float) = 0
		[Toggle]_DisplayAlpha("Display Alpha", Float) = 0
		[Toggle]_DisplayAllVC("Display All VC", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float4 vertexColor : COLOR;
		};

		uniform float _DisplayAlpha;
		uniform float _DisplayAllVC;
		uniform float _DisplaySingleRGB;
		uniform float _R;
		uniform float _G;
		uniform float _B;
		uniform float _SingleRGBMode;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float temp_output_73_0 = ceil( _DisplaySingleRGB );
			float3 appendResult49 = (float3(( temp_output_73_0 + 0 ) , ( temp_output_73_0 + -1 ) , ( temp_output_73_0 + -2 )));
			float3 clampResult74 = clamp( appendResult49 , float3( 0,0,0 ) , float3( 1,1,0 ) );
			float3 layeredBlendVar31 = clampResult74;
			float layeredBlend31 = ( lerp( lerp( lerp( i.vertexColor.r , i.vertexColor.g , layeredBlendVar31.x ) , i.vertexColor.b , layeredBlendVar31.y ) , 0 , layeredBlendVar31.z ) );
			float3 temp_cast_0 = (layeredBlend31).xxx;
			float3 appendResult81 = (float3(( i.vertexColor.r * lerp(0,1,_R) ) , ( i.vertexColor.g * lerp(0,1,_G) ) , ( i.vertexColor.b * lerp(0,1,_B) )));
			float3 lerpResult89 = lerp( temp_cast_0 , appendResult81 , lerp(1,0,_SingleRGBMode));
			float4 temp_cast_2 = (i.vertexColor.a).xxxx;
			o.Emission = pow( lerp(lerp(float4( lerpResult89 , 0.0 ),i.vertexColor,_DisplayAllVC),temp_cast_2,_DisplayAlpha) , 2.2 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
1927;29;1906;1004;299.7256;58.96851;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;32;-1915.831,-273.131;Float;False;Property;_DisplaySingleRGB;Display Single RGB;1;1;[IntRange];Create;True;0;0;0;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.CeilOpNode;73;-1559.27,-269.6142;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;-1301.884,-179.4745;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-1301.877,-272.2834;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-1305.136,-357.9328;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;84;-1200.059,334.9337;Float;False;Property;_B;B;4;0;Create;True;0;0;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;49;-1068.248,-290.0351;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;78;-1200.198,-55.88585;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;82;-1200.059,141.5867;Float;False;Property;_R;R;2;0;Create;True;0;0;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;83;-1202.705,239.4966;Float;False;Property;_G;G;3;0;Create;True;0;0;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;87;-883.2458,255.176;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;74;-893.7355,-293.3957;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;-879.9996,164.282;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;-879.9996,73.38786;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;88;-48.14681,118.6053;Float;False;Property;_SingleRGBMode;Single RGB Mode;0;0;Create;True;0;1;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LayeredBlendNode;31;-481.9726,-231.3158;Float;False;6;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;81;-77.8568,-23.30473;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;89;299.1984,-6.373952;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;79;546.0551,311.2999;Float;False;Property;_DisplayAllVC;Display All VC;6;0;Create;True;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;92;794.8149,395.6798;Float;False;Property;_DisplayAlpha;Display Alpha;5;0;Create;True;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;90;1047.392,321.4946;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;2.2;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;80;1299.812,291.1336;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Debug/Display VC;False;False;False;False;True;True;True;True;True;True;True;True;False;False;False;False;False;False;False;Off;0;0;False;0;0;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;73;0;32;0
WireConnection;66;0;73;0
WireConnection;60;0;73;0
WireConnection;64;0;73;0
WireConnection;49;0;64;0
WireConnection;49;1;60;0
WireConnection;49;2;66;0
WireConnection;87;0;78;3
WireConnection;87;1;84;0
WireConnection;74;0;49;0
WireConnection;86;0;78;2
WireConnection;86;1;83;0
WireConnection;85;0;78;1
WireConnection;85;1;82;0
WireConnection;31;0;74;0
WireConnection;31;1;78;1
WireConnection;31;2;78;2
WireConnection;31;3;78;3
WireConnection;81;0;85;0
WireConnection;81;1;86;0
WireConnection;81;2;87;0
WireConnection;89;0;31;0
WireConnection;89;1;81;0
WireConnection;89;2;88;0
WireConnection;79;0;89;0
WireConnection;79;1;78;0
WireConnection;92;0;79;0
WireConnection;92;1;78;4
WireConnection;90;0;92;0
WireConnection;80;2;90;0
ASEEND*/
//CHKSM=5FD4AD640F92A7391AAAE52D72FC301A51D52932