// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Debug/Display Camera Grid"
{
	Properties
	{
		_MainTex ( "Screen", 2D ) = "black" {}
		[HDR]_Color("Color", Color) = (0.5073529,0.5073529,0.5073529,0)
		[Toggle]_EvenGridToggle("Even Grid Toggle", Float) = 0
		_YGrid("Y Grid", Float) = 4
		_XGrid("X Grid", Float) = 4
		_EvenGrid("Even Grid", Float) = 2
		_LinesStep("Lines Step", Float) = 0.99
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		
		
		ZTest Always
		Cull Off
		ZWrite Off
		

		Pass
		{ 
			CGPROGRAM 

			#pragma vertex vert_img_custom 
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata_img_custom
			{
				float4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				
			};

			struct v2f_img_custom
			{
				float4 pos : SV_POSITION;
				half2 uv   : TEXCOORD0;
				half2 stereoUV : TEXCOORD2;
		#if UNITY_UV_STARTS_AT_TOP
				half4 uv2 : TEXCOORD1;
				half4 stereoUV2 : TEXCOORD3;
		#endif
				float4 ase_texcoord4 : TEXCOORD4;
			};

			uniform sampler2D _MainTex;
			uniform half4 _MainTex_TexelSize;
			uniform half4 _MainTex_ST;
			
			uniform float4 _Color;
			uniform float _EvenGridToggle;
			uniform float _XGrid;
			uniform float _LinesStep;
			uniform float _YGrid;
			uniform float _EvenGrid;
			inline float4 ASE_ComputeGrabScreenPos( float4 pos )
			{
				#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
				#else
				float scale = 1.0;
				#endif
				float4 o = pos;
				o.y = pos.w * 0.5f;
				o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
				return o;
			}
			

			v2f_img_custom vert_img_custom ( appdata_img_custom v  )
			{
				v2f_img_custom o;
				float4 ase_clipPos = UnityObjectToClipPos(v.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord4 = screenPos;
				
				o.pos = UnityObjectToClipPos ( v.vertex );
				o.uv = float4( v.texcoord.xy, 1, 1 );

				#if UNITY_UV_STARTS_AT_TOP
					o.uv2 = float4( v.texcoord.xy, 1, 1 );
					o.stereoUV2 = UnityStereoScreenSpaceUVAdjust ( o.uv2, _MainTex_ST );

					if ( _MainTex_TexelSize.y < 0.0 )
						o.uv.y = 1.0 - o.uv.y;
				#endif
				o.stereoUV = UnityStereoScreenSpaceUVAdjust ( o.uv, _MainTex_ST );
				return o;
			}

			half4 frag ( v2f_img_custom i ) : SV_Target
			{
				#ifdef UNITY_UV_STARTS_AT_TOP
					half2 uv = i.uv2;
					half2 stereoUV = i.stereoUV2;
				#else
					half2 uv = i.uv;
					half2 stereoUV = i.stereoUV;
				#endif	
				
				half4 finalColor;

				// ase common template code
				float2 uv_MainTex = i.uv.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode158 = tex2D( _MainTex, uv_MainTex );
				float4 screenPos = i.ase_texcoord4;
				float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( screenPos );
				float4 temp_output_151_0 = ( ase_grabScreenPos * _ScreenParams * ( _EvenGrid / 1000.0 ) );
				float clampResult134 = clamp( ( _Color.a + lerp(( step( frac( ( screenPos.x * _XGrid ) ) , _LinesStep ) * step( frac( ( screenPos.y * _YGrid ) ) , _LinesStep ) ),( step( frac( (temp_output_151_0).x ) , _LinesStep ) * step( frac( (temp_output_151_0).y ) , _LinesStep ) ),_EvenGridToggle) ) , 0.0 , 1.0 );
				float4 lerpResult113 = lerp( ( _Color * tex2DNode158 ) , tex2DNode158 , clampResult134);
				

				finalColor = lerpResult113;

				return finalColor;
			} 
			ENDCG 
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15301
1927;29;1906;1004;-763.3305;113.6093;1.745656;True;False
Node;AmplifyShaderEditor.RangedFloatNode;139;281.2314,1004.658;Float;False;Property;_EvenGrid;Even Grid;4;0;Create;True;0;0;False;0;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;147;232.2278,1097.964;Float;False;Constant;_EvenGridScale;Even Grid Scale;6;0;Create;True;0;0;False;0;1000;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenParams;135;252.6073,827.9012;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;148;743.5157,937.5295;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GrabScreenPosition;150;190.2701,657.0457;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;98;227.3471,299.3602;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;104;279.7103,501.3884;Float;False;Property;_XGrid;X Grid;3;0;Create;True;0;0;False;0;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;528.4236,768.5159;Float;False;3;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;105;279.289,578.9666;Float;False;Property;_YGrid;Y Grid;2;0;Create;True;0;0;False;0;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;141;901.3544,792.1802;Float;False;True;False;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;152;907.3172,878.5096;Float;False;False;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;103;550.7103,422.3884;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;560.7103,563.3884;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;111;734.076,410.7924;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;112;726.076,557.7924;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;145;1129.699,798.0249;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;108;739.7103,632.3884;Float;False;Property;_LinesStep;Lines Step;5;0;Create;True;0;0;False;0;0.99;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;153;1133.823,882.449;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;107;1054.728,446.3728;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;109;1048.502,556.5994;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;156;1497.679,856.9692;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;155;1493.78,765.9691;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;154;1639.487,814.7256;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;110;1244.71,489.3884;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;114;1889.581,638.3386;Float;False;Property;_EvenGridToggle;Even Grid Toggle;1;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;161;1807.013,212.6217;Float;False;Property;_Color;Color;0;1;[HDR];Create;True;0;0;False;0;0.5073529,0.5073529,0.5073529,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;157;1699.09,428.3033;Float;False;0;0;_MainTex;Shader;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;166;2212.97,586.3568;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;158;1898.602,423.0916;Float;True;Property;_TextureSample0;Texture Sample 0;6;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;134;2359.849,602.4581;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;159;2263.568,354.8438;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;167;526.5527,1114.408;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;113;2551.632,461.9594;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;93;2866.338,481.8288;Float;False;True;2;Float;ASEMaterialInspector;0;1;Debug/Display Camera Grid;c71b220b631b6344493ea3cf87110c93;0;0;;1;False;False;True;Off;False;False;True;2;True;7;False;True;0;False;0;0;0;False;False;False;False;False;False;False;False;False;True;2;0;0;0;1;0;FLOAT4;0,0,0,0;False;0
WireConnection;148;0;139;0
WireConnection;148;1;147;0
WireConnection;151;0;150;0
WireConnection;151;1;135;0
WireConnection;151;2;148;0
WireConnection;141;0;151;0
WireConnection;152;0;151;0
WireConnection;103;0;98;1
WireConnection;103;1;104;0
WireConnection;106;0;98;2
WireConnection;106;1;105;0
WireConnection;111;0;103;0
WireConnection;112;0;106;0
WireConnection;145;0;141;0
WireConnection;153;0;152;0
WireConnection;107;0;111;0
WireConnection;107;1;108;0
WireConnection;109;0;112;0
WireConnection;109;1;108;0
WireConnection;156;0;153;0
WireConnection;156;1;108;0
WireConnection;155;0;145;0
WireConnection;155;1;108;0
WireConnection;154;0;155;0
WireConnection;154;1;156;0
WireConnection;110;0;107;0
WireConnection;110;1;109;0
WireConnection;114;0;110;0
WireConnection;114;1;154;0
WireConnection;166;0;161;4
WireConnection;166;1;114;0
WireConnection;158;0;157;0
WireConnection;134;0;166;0
WireConnection;159;0;161;0
WireConnection;159;1;158;0
WireConnection;167;0;139;0
WireConnection;167;1;147;0
WireConnection;113;0;159;0
WireConnection;113;1;158;0
WireConnection;113;2;134;0
WireConnection;93;0;113;0
ASEEND*/
//CHKSM=78E7FDB02E743EC6AEBA54E8DF379C5217E19358